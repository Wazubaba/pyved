Changelog
=========

03-13-2014
----------
+	Started implementation of the active data class, though this is more of a test to determine how we will approach this.
+	Debating on making a few basic types.

03-09-2014
----------
+	Did some research on what method to use for the VED format.
+	Jotted down random assorted notes that probably are common sense or complete dribble to anyone but Wazubaba.
+	Added a few tests from playing with data formats. Wazubaba accidentally deleted the main one while testing the main .gitignore... Not too much loss, it was an attempt to hack in struct generation to the old Ved class that was only half-implemented due to the need to seriously plan the data format more.
+	Moved gui tests to a specific gui directory under testing.
+	Not a whole lot of code this update, most of the work was more planning for the future. :( But hey, the night is young :3

03-08-2014
----------
+	Optimized the io.User class a bit
+	Attempting to make file-type plugins relatively easy to work with
+	Created test program for the ved data format (Our future save format :D)
+	Added an albeit pretty much blank main source file for pyved \o/.

03-07-2014
----------
+	Started sorting of the project.
+	Implemented generic class for all IO-using functions
+	Started implementation of VED file class
+	Added Garmine to the legalese nonsense as he is a co-author


03-06-2014
----------
+	Added "errytang" to the git repo. Going to need to clean it a bit later, but for now there really isn't enough crap to warrant it :/
+	So far we have: a config parser, a basic, partially implemented cube data array. Partial importing and parsing of a "pallette" file.
+	An icon
+	A very very WIP GUI, see guitest.py
+	Random scattered crap that is pretty much just implementation tests and residue from previous grrs...
+	A pretty good intent to actually *gasp* DOCUMENT THE DAMN PROJECT O_O
+	Pertaining to the former entry, a very early man page is included, it is utterly useless atm though, as there really is nothing made except a few of the libraries.
+	Actually made the markdown bullet lists properly (I hope...)
