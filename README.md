PYVED	-	The Python Voxel EDitor
================================================================================

PyVed is intended to be the primary voxel editor for the upcoming space-sim
[Scrumble Ship](scrumbleship.com)

However, this does not mean that it will be limited to only that, with any
luck, we will also support a few more useful formats.

REQUIREMENTS
--------------------------------------------------------------------------------
PyVed will depend on:

+	gtk2
+	glade - maybe, idk if this is still needed or not...
+	opengl
+	python version > 2.5 and < 3.0
+	most likely opengl someday :P

Detailed instructions for installation will come once we actually have
something to install :P


IDEAS
-------------------------------------------------------------------------------

Consider this sorta a wish-list for now...

+	project files, aka groups of different voxels in one file +palette I/O, to save color groups
+	support for multiple different formats, including the extremely antiquated slab9(or was it 6...) format
+	support for obj(might not be too hard, obj is well documented, optimizing it however would be a nightmare...)
+	theme support(probably won't be needed since we will be using gtk)
+	viewport color settings(for changing color of say... background, grids, cursor, whatever the damn can be colored ffs...)
+	3d cursor position grid(think like vim's coloumn and row lines, only with depth too...)
+	voxel grid(basically take the layer fill from sproxel, only use that to overlay a grid on the selected axis)
+	toggles for different axis locks
+	modding hooks(already have done some experimentation here prior, *MIGHT* be possible...)


