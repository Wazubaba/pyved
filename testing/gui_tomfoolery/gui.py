import gtk

class test:
    def destroy(self,object,data=None): gtk.main_quit()

    def __init__(self):
        self.guidef = "pyved.glade"
        self.builder = gtk.Builder()
        self.builder.add_from_file(self.guidef)

        self.builder.connect_signals(self)

        self.window = self.builder.get_object("MainWindow")
        self.window.show()

if __name__ == "__main__":
    interface = test()
    gtk.main()
