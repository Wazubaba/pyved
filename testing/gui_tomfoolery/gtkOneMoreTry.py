import sys
try:
    import pygtk
    pygtk.require("2.0")
except:
    pass
try:
    import gtk
    import gtk.glade
except:
    sys.exit(1)

class test:
    def __init__(self):
        self.gladefile = "pleaseWork.glade"
        self.wtree = gtk.glade.XML(self.gladefile)

        self.window = self.wtree.get_widget("MainWindow")
        if (self.window):
            self.window.connect("destroy", gtk.main_quit)

if __name__ == "__main__":
    stuffs = test()
    gtk.main()
