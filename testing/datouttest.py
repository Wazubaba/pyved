import struct
import binascii

rfh = open("raw-file",'w')
bfh = open("binary-file",'wb')

size = (15,15,15)

s = struct.Struct("3i")
bdat = s.pack(*size)

# write raw text file
for val in size[:2]: rfh.write(str(val)+',')
rfh.write(str(size[2]))
rfh.close()

bfh.write(bdat)
bfh.close()

print("Operation Complete.")
