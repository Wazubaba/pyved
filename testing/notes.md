Realization
===========
Okay, so we need to seriously plan this data format. Like once and for all
needed...

projects
--------
Projects are dictionaries. They contain name-based entries of voxel node
maps(vnm).

vnm
---
A vnm is a tuple. First entry is size, should size be it's own type, or just a
simple tuple with 3 entries? Second entry is the actual data.

datastorage
-----------
Odds are good binary storage would be best. It would make life a huge amount
easier.

However...

Python's struct module requires a "decoder" string to parse the data. I am
uncertain whether there is a way to aquire this automatically.

One thought, is to simply have each save be a gziped archive, inside of
which is the raw struct data, as well as a simple plain-text file. This
plain-text contains the string used to decode the file.

There is potentially an alternative solution as well. A seperate key for the
two/three types. There is a flaw however. How does one determine the size of
the map?

My proposal, would be to have simply one file, and this would be merged
binary data. aka two files in one. Map sizes will *ALWAYS* be 3 integers.

So yes, this will take a small amount of planning and experimentation.
After a test, it appears writing the sizes via plain text will actually take
less HDD space than generating binary data.



Data Format
-----------

```
sizex,sizey,sizez\n
binary data for project 1\n
binary data for project 2\n
etc...
```

*Example of 4 VNM project file:*
```
16,16,16
gobbledegook 
moarnonsense
ohaithisisntwhereiparked
aughmyfaceisonfireitseatingmyheadohgodhaaaalp
```

