# This will handle I/O on the pyved save format, as well as potentially
#   store our data matrix.

# NOTES
# Our save format will be binary to save on sizes, so going to be rigging up
#   struct parsers...
#
# okay, so for now...
#
# rough binary mapping for our save format: (yes, I am referencing the current
#   files a bit heavilly :P
#   string "pyved save format"
#   float save file version number (might be useful if we change something big
#       and need to throw a converter to the new format in
#   int numObjs (how many voxel models exist in this project file
#   int objnum (index of the object)
#   int sizex
#   int sizey
#   int sizez size of the model
#   char mode 1 or 2, 1 = rgb aka larger mode, 2 = indexed, aka palette mode
#       aka not stupidly large mode :P
#
#   if 1:
#   giant massive fucking obnoxiously large list of:
#       char r
#       char g
#       char b
#       char a
#   looped till size x,y,z is reached
#   if 2:
#   
#      char index
#   looped till size x,y,z is reached
#   these are the meat and potatoes of our save file, the actual bloody
#       models :P
#   actually, not entirely sure if saving via rgb mode is even worth-while
#       unless one is exporting, and then you'd use a different
#       format... but wtf, may as well attempt to offer it :P
#
#   There is but ONE(1) issue with palette format: limit of 255 colors with 0
#       being empty. other than that, it is far better :P
#
#   And that is why we can include standard rgb anyways :P
#
#   annendum: char mode 3: hybrid mode. each voxel will have two chars, index,
#       and alpha. This way, you can use a slider to define alpha levels, but
#       still use half the chars of a standard rgb model \o/
#
#

import sys
from PIL import Image as image # I am not using shift every bloody time >_<

class vem:
    def __init__(self):
        self.data = []
        self.size = (15,15,15) # scrumble default is 16^3
        self.pallette = {} # Using a dict to do a mapping for indexes.
        self.loadPallette("res/default-pallette.png")
        self.new()
        

    def loadPallette(self,fn=""):
        im = image.open(fn)
#        im = image.open(open(fn,"rb"))
        try:
            im = image.open(fn)
            if im.mode != "RGB":
                print("[pyved.io - pallette(%s) not in rgb mode, converting...]" %(fn))
                im.convert("RGB")
            pixmap = im.load()

            index = 0
            for y in range(0, im.size[1]):
                for x in range(0, im.size[0]):
#                    rgba = pixmap[x,y]
                    self.pallette[str(index)] = pixmap[x,y]
                    index += 1
#                    print(rgba)

        except IOError:
            print("[pyved.io - Unable to load pallette(%s)...]" %(fn))

    def new(self):
        for z in range(0, self.size[2]):
            self.data.append([])
            for y in range(0, self.size[1]):
                self.data[z].append([])
                for x in range(0, self.size[0]):

                    self.data[z][y].append(0) # gen empty voxels

    def debug(self): # dumps the voxel model into console, seperated by Z, with grids of Y,X
        for z in range(0, self.size[2]):
            sys.stdout.write("\n------------")
            for y in range(0, self.size[1]):
                sys.stdout.write("\n")
                for x in range(0, self.size[0]):
                    sys.stdout.write("%i " %self.data[z][y][x])

        sys.stdout.write("\n") # for cleanliness

        for inc in range(0, 255):
                print("[%i,%i,%i]  " %(self.pallette[str(inc)]))

test = vem()
test.loadPallette("default-pallette.png")
test.new()
test.debug()
