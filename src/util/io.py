class User(object):
    """
	A simple generic IO class to be inherited for genericish functions.

	The only really important var for this that should be set in the
	inheritor's class is "fname", which should be a string containing the
	target filename.

	The inheritor should also have a function called export(fh), where fh
	is a standard python file handle (aka file pointer).

	Please note: your export function doesn't need to close the file.
    """
#   def __init__(self):
#   NOTE might need to add this section for important vars

    def fileExists(self):
        """
	    Tests if a file exists, and returns -1 if it can be written or
	    does not, and 0 if it is not allowed to be over-written.
        """
        # NOTE clear to write == -1, 0 == gitoutmahfaec
        try:
            testfh = open(self.fname, 'r')
            testfh.close()
            if self.writePrompt(): return -1
            else: return 0
        except IOError: return -1
        
    def writePrompt(self):
        """
            Eventually this will fire a GUI prompt to ask if a user is certain
            they wish to over-write an existing file. ATM it just asks for a y
            to confirm.
        """
        print("[PLACEHOLDER]Are you sure you wish to overwrite?")
        # TODO Use GUI popup for confirmation
        if raw_input("y/N:").lower() == "y":
            return 1
        else:
            return 0

    def write(self):
        """
            this is runs the file through the fileExists function, then
            executes a callback (self.export(fh)) where fh is a normal python
            file pointer.

            Basically, just have a function called export in the class that
            inherits this one and takes a file pointer for args, and that gets
            called.
        """
        if self.fileExists() == -1:
            fh = open(self.fname,'w')
            self.export(fh)
	    fh.close()
