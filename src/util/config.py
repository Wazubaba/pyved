#pyved config parser
# TODO rewrite this entire file to use the new User class
class config:
	"""
		This is essentially the config sortaish...
	"""
	def __init__(self,verbose = False):
		"""
			Config class entry point.
		"""
		self.data = {}
		self.loaded = False
		self.verbose = verbose

	def open(self,fh):
		"""
			Open a config, and parse it into self.data, offering a report should there be any malformed lines.
		"""
		lineNum = 1 # start at one since this is for a debug message
		self.data = {} # reset the data block in case we are reloading
		try: # ensure that target file exists :P
			self.file = open(fh,"r")
		except IOError:
			if self.verbose: print("[pyved.config - unable to open config(%s)...]" %(fh))
			return -1
		for ln in self.file.readlines():
			try:
				if ln == "\r\n": continue # deal with stupid windows nonsense
				if ln == "\n": continue # skip line breaks for sorting

				key,val = ln.split("=",1)

				# formatting...
				key = key.strip(" ")
				val = val.strip("\r\n") # strip off the newline chars
				val = val.strip(" ") # strip off surrounding spaces

				try:
					val = float(val) # can we even convert to a number
					try: val = int(val) # okay, can we convert to an int?
					except ValueError: pass # guess not
				except ValueError: pass # I give up, congrats on your new string :P

				self.data[key]=val
				
			except ValueError:
				# not sure if this should be under verbose mode or not :/...
				print("[pyved.config - malformed line in config(%s)@line %i, ignoring...]" %(fh, lineNum))
			lineNum += 1
		self.file.close()
		self.loaded = True

	def save(self,fh):
		"""
			Write out a config, should only error if there is a permissions issue or you are somehow utterly out of space.
		"""
		try:
			self.file = open(fh,"w")
		except IOError:
			print("[pyved.config ! Unable to write config(%s) !]" %(fh))
			return -1
		for key in self.data:
			self.file.write("%s = %s\n" %(key,self.data[key]))


"""
	Test harness
"""


test = config()
test.open("settings.cfg")
for key in test.data:
	print("%s = %s" %(key,test.data[key]))

print("\n----------\n")
test.data["owmebum"] = "hey look a chicken..."
test.save("newsettings.cfg")
test.open("newsettings.cfg")
for key in test.data:
	print("%s = %s" %(key,test.data[key]))

#"""
