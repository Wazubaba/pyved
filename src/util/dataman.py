#preliminary data class
#TODO - Fill with utilites for affecting the vnm

from sys import stdout

class NoData(Exception):
    def __init__(self, val="No data exists in this Voxel Node Map."):
        self.val = val
    def __str__(self):
        return repr(self.val)


class VNM(object):
    def __init__(self,data = [], size = (16,16,16)):
        self.data = data
        self.size = size

    def newMap(self):
        for z in range(0,self.size[0]):
            self.data.append([])
            for y in range(0, self.size[1]):
                self.data[z].append([])
                for x in range(0, self.size[2]):
                    self.data[z][y].append((0,0,0,0))

    def __nonzero__(self):
        if len(self.data) > 0: return True
        else: return False

    def debugView(self):
        if not self.__nonzero__(): raise NoData # if there actually is data...
        for z in range(0,self.size[0]):
            stdout.write("\n---------")
            for y in range(0, self.size[1]):
                stdout.write("\n")
                for x in range(0, self.size[2]):
                    stdout.write("(%i,%i,%i,%i) " %self.data[z][y][x])
                    
        stdout.write("\n")


test = VNM()
test.newMap()
test.debugView()

