# All IO involving the ved file format is figured out here
from util import io

class File(io.User):
    """
            This class contains everything about the VED file format that is needed to
        output that file-type.
    """
    def __init__(self,fname,fdat):
        """
            Init function, this configures the file name and its data.
        """
        self.fname = fname
        self.fdat = fdat

    def export(self,fh):
        """
        The export function, which is called by the write call for this format.
        """
        # TODO add stuff here to output the data
        fh.write(self.fdat+"\n")
        # NOTE We don't need to close the file, all this function needs do is
        # output all the data. the io.User class will automatically close the
        # file once this function returns :P

